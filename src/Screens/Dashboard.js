import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, Animated, Dimensions } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../Assets/dsc-logo.png';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Spinner } from "native-base";
import Ripple from "react-native-material-ripple";
import CalendarStrip from 'react-native-calendar-strip';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';
import { Shadow } from 'react-native-neomorph-shadows';
import { dashboardStyles } from '../CommonComponents/css';

import FuelCost from '../Assets/FuelCost.png';
import RepairCost from '../Assets/RepairCost.png';
import TotalTrucks from '../Assets/TotalTrucks.png';
import TotalTrailer from '../Assets/TotalTrailer.png';
import PassedStud from '../Assets/PassedStud.png';
import SalesMonth from '../Assets/SalesMonth.png';
import { Avatar, normalize } from 'react-native-elements';
import Header from '../CommonComponents/Header';

export default class Dashboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            eff: false,
            showProfile: false,
            isPress: true,
            weekPrees: false,
            monthPress: false,
            modalVisible: false,
            open: false,
            SlideInLeft: new Animated.Value(0),
            slideUpValue: new Animated.Value(0),
            fadeValue: new Animated.Value(0),
            selectedDate: '',
            date: new Date(),
            list1: [
                {
                    name: 'Sales of Month',
                    avatar: SalesMonth,
                    price: '$10000'
                },
                {
                    name: 'Monthly Fuel Cost',
                    avatar: FuelCost,
                    price: '$15000'
                }

            ],
            list2: [
                {
                    name: 'Repair Cost',
                    avatar: RepairCost,
                    price: '$8000'
                },
                {
                    name: 'Passed Students',
                    avatar: PassedStud,
                    price: '15'
                }

            ],
            list3: [
                {
                    name: 'Total Healthy Truck',
                    avatar: TotalTrucks,
                    price: '6'
                },
                {
                    name: 'Total Healthy Trailer',
                    avatar: TotalTrailer,
                    price: '4'
                },

            ]
        }
        Animated.parallel([
            Animated.timing(this.state.SlideInLeft, {
                toValue: 1,
                duration: 500,
                useNativeDriver: true
            }),
            Animated.timing(this.state.fadeValue, {
                toValue: 1,
                duration: 500,
                useNativeDriver: true
            }),
            Animated.timing(this.state.slideUpValue, {
                toValue: 1,
                duration: 500,
                useNativeDriver: true
            })
        ]).start();

    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    logOut = () => {
        AsyncStorage.clear();
        this.props.navigation.replace('Login');
    }

    render() {
        const { modalVisible } = this.state;
        var touchProps = {
            style: this.state.isPress === true ? dashboardStyles.btnPress : dashboardStyles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ isPress: !this.state.isPress, weekPrees: false, monthPress: false }),                 // <-- "onPress" is apparently required
        };
        var weekProps = {
            style: this.state.weekPrees === true ? dashboardStyles.btnPress : dashboardStyles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ weekPrees: !this.state.weekPrees, isPress: false, monthPress: false }),                 // <-- "onPress" is apparently required
        };
        var monthProps = {
            style: this.state.monthPress === true ? dashboardStyles.btnPress : dashboardStyles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ monthPress: !this.state.monthPress, weekPrees: false, isPress: false }),                 // <-- "onPress" is apparently required
        };

        let { slideUpValue, fadeValue, SlideInLeft } = this.state;

        return (
            <View style={dashboardStyles.container}>
                <View style={dashboardStyles.head}>
                    <View style={dashboardStyles.imgView}>
                        <TouchableOpacity onPress={() => this.logOut()}>
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name="logout" color='grey' size={24} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={dashboardStyles.title}>
                        Blue Mountain Driving School
                    </Text>
                    <Image style={dashboardStyles.image} source={logo} />
                </View>

                {/* <View style={{ flexDirection: 'row', width: wp("90%"), justifyContent: "flex-end" }}>
                    <View style={dashboardStyles.listView}>
                        <Text style={dashboardStyles.headText}>Dashboard</Text>
                    </View>

                    <Avatar
                        containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop: 5 }}
                        rounded
                        icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                        onAccessoryPress={() => Alert.alert("change avatar")}
                        overlayContainerStyle={{ backgroundColor: 'grey' }}
                        size={25}
                        onPress={() => this.setState({ showProfile: !this.state.showProfile })}
                    />
                    {this.state.showProfile === true ?
                        <View style={dashboardStyles.profileView}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Avatar
                                        containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                        rounded
                                        icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                        onAccessoryPress={() => Alert.alert("change avatar")}
                                        overlayContainerStyle={{ backgroundColor: 'grey' }}
                                        size={15}
                                        onPress={() => console.log("Works!")}
                                    />
                                    <Text style={{ color: 'grey', paddingLeft: 5 }}>Profile</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{ borderWidth: 1, borderColor: 'grey' }} />
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Icon name="log-out" style={{ paddingTop: 3, }} color='grey' size={15} />
                                    <Text style={{ color: 'grey', paddingLeft: 5 }}>Log Out</Text>
                                </View>
                            </TouchableOpacity>
                        </View> : null}

                </View> */}

                <ScrollView showsVerticalScrollIndicator={false}>
                    <CalendarStrip
                        scrollable
                        calendarAnimation={{ type: 'sequence', duration: 30 }}
                        selectedDate={this.state.date}
                        onDateSelected={date => this.setState({ selectedDate: date })}
                        daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#00a64f' }}
                        style={dashboardStyles.celendarCom}
                        calendarHeaderStyle={{ color: 'black', paddingTop: 10, fontSize: 15 }}
                        calendarColor={'#7743CE'}
                        dateNumberStyle={{ color: 'black' }}
                        dateNameStyle={{ color: 'black' }}
                        highlightDateNumberStyle={{ color: '#00a64f' }}
                        highlightDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNumberStyle={{ color: '#00a64f' }}
                        iconContainer={{ flex: 0.1 }}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: '5%' }}>

                        <TouchableOpacity {...touchProps}>
                            <Text style={this.state.isPress === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Day</Text>
                        </TouchableOpacity>

                        <TouchableOpacity {...weekProps}>
                            <Text style={this.state.weekPrees === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Week</Text>
                        </TouchableOpacity>
                        <TouchableOpacity {...monthProps}>
                            <Text style={this.state.monthPress === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Month</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setModalVisible(!modalVisible)}>
                            <View style={{ borderWidth: 1, width: wp('10%'), alignItems: 'center', padding: 5, borderRadius: 3, borderColor: '#feba24' }}>
                                <FontAwesome5 name="calendar" color='#feba24' size={25} />
                            </View>
                        </TouchableOpacity>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                                this.setModalVisible(!modalVisible);
                            }}
                        >
                            <View style={dashboardStyles.modalView}>
                                <View style={{ backgroundColor: 'white' }}>
                                    <Text style={dashboardStyles.modalText}>Custom Date</Text>
                                </View>
                                <View style={{ width: wp('85%'), height: hp('35%') }}>
                                    <Text style={dashboardStyles.modalLable}>From Date</Text>
                                    <DatePicker
                                        style={{ width: wp('85%'), padding: 10 }}
                                        date={this.state.fromDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        useNativeDriver={true}
                                        customStyles={{
                                            dateInput: {
                                                borderColor: 'white',
                                                borderBottomColor: 'black'
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => { this.setState({ fromDate: date }) }}
                                    />
                                    <Text style={dashboardStyles.modalLable}>To Date</Text>
                                    <DatePicker
                                        style={{ width: wp('85%'), padding: 10 }}
                                        date={this.state.toDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        useNativeDriver={true}
                                        customStyles={{
                                            dateInput: {
                                                borderColor: 'white',
                                                borderBottomColor: 'black',
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => { this.setState({ toDate: date }) }}
                                    />
                                    <Ripple
                                        rippleColor="#99FF99"
                                        onPress={() => this.setModalVisible(!modalVisible)}
                                        style={dashboardStyles.loginBtn}
                                    >
                                        <Text style={dashboardStyles.loginText}>Submit</Text>
                                    </Ripple>
                                </View>

                            </View>
                        </Modal>

                    </View>

                    <View style={dashboardStyles.mainView}>

                        <View style={{ flexDirection: 'column' }}>

                            <Animated.View
                                style={{
                                    transform: [
                                        {
                                            translateX: slideUpValue.interpolate({
                                                inputRange: [0, 1],
                                                outputRange: [-600, 0]
                                            })
                                        }
                                    ],
                                    flex: 1,
                                    height: 200,
                                    width: 150,
                                    borderRadius: 12,
                                    justifyContent: "center"
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    {this.state.list1.map((u, i) => {
                                        return (
                                            <Shadow key={i} style={dashboardStyles.itemView}>
                                                <Image style={{ top: '10%', width: "30%", height: "30%" }} source={u.avatar} />
                                                <Text style={{ paddingTop: '20%', color: 'black', fontSize: normalize(12), bottom: '5%' }}>{u.name}</Text>
                                                <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                                <Text style={dashboardStyles.priceText}>{u.price}</Text>
                                            </Shadow>
                                        );
                                    })}
                                </View>
                            </Animated.View>

                            <Animated.View
                                style={{
                                    transform: [
                                        {
                                            translateY: SlideInLeft.interpolate({
                                                inputRange: [0, 1],
                                                outputRange: [600, 0]
                                            })
                                        }
                                    ],
                                    flex: 1,
                                    height: 200,
                                    width: 150,
                                    borderRadius: 12,
                                    justifyContent: "center"
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    {this.state.list2.map((u, i) => {
                                        return (
                                            <Shadow key={i} style={dashboardStyles.itemView}>
                                                <Image style={{ top: '10%', width: "28%", height: "35%" }} source={u.avatar} />
                                                <Text style={{ paddingTop: '20%', color: 'black', fontSize: normalize(12), bottom: '5%' }}>{u.name}</Text>
                                                <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                                <Text style={dashboardStyles.priceText}>{u.price}</Text>
                                            </Shadow>
                                        );
                                    })}
                                </View>
                            </Animated.View>

                            <Animated.View
                                style={{
                                    opacity: fadeValue,
                                    flex: 1,
                                    height: 200,
                                    width: 150,
                                    borderRadius: 12,
                                    justifyContent: "center"
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    {this.state.list3.map((u, i) => {
                                        return (
                                            <Shadow key={i} style={dashboardStyles.itemView}>
                                                <Image style={{ top: '10%' }} source={u.avatar} />
                                                <Text style={{ paddingTop: '20%', color: 'black', fontSize: normalize(12), bottom: '5%' }}>{u.name}</Text>
                                                <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                                <Text style={dashboardStyles.priceText}>{u.price}</Text>
                                            </Shadow>
                                        );
                                    })}
                                </View>
                            </Animated.View>
                        </View>
                    </View>
                </ScrollView>
            </View >
        )
    }
}