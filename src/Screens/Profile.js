import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  DatePicker,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Avatar } from "react-native-elements";
import Header from "../CommonComponents/Header";
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-crop-picker";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fname: "",
      mname: "",
      lname: "",
      dob: "",
      mobile: "",
      email: "",
      addres: "",
    };
    this.takePhotoFromCamera = this.takePhotoFromCamera.bind(this);
  }

  takePhotoFromCamera = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      console.log(image);
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Header title={"Edit Profile"} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.mainView}>
            <Avatar
              containerStyle={{ alignSelf: "center", backgroundColor: "grey" }}
              rounded
              icon={{ name: "user-alt", type: "font-awesome-5" }}
              onAccessoryPress={() => Alert.alert("change avatar")}
              overlayContainerStyle={{ backgroundColor: "grey" }}
              size={hp("15%")}
            >
              <Avatar.Accessory size={28} onPress={this.takePhotoFromCamera} />
            </Avatar>
            <Text
              style={{ alignSelf: "center", paddingTop: 10, color: "grey" }}
            >
              Change Profile Picture
            </Text>
            <View>
              <Text style={{ color: "black" }}>Student Id</Text>
              <Text style={{ color: "grey" }}>#1234</Text>
            </View>
            <View style={{ paddingTop: 10 }}>
              <Text style={{ paddingTop: 5, color: "black" }}>First Name</Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ fname: text })}
                placeholder="Smith"
                underlineColorAndroid="transparent"
                value={this.state.fname}
              />
              <Text style={{ paddingTop: 10, color: "black" }}>
                Middle Name
              </Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ mname: text })}
                placeholder="Hery"
                underlineColorAndroid="transparent"
                value={this.state.mname}
              />
              <Text style={{ paddingTop: 10, color: "black" }}>Last Name</Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ lname: text })}
                placeholder="Joe"
                underlineColorAndroid="transparent"
                value={this.state.lname}
              />
              <Text style={{ paddingTop: 10, color: "black" }}>
                Date of Birth
              </Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ dob: text })}
                placeholder="24/08/1996"
                underlineColorAndroid="transparent"
                value={this.state.dob}
              />
              <Text style={{ paddingTop: 10, color: "black" }}>Mobile No</Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ mobile: text })}
                placeholder="+91 1234567809"
                underlineColorAndroid="transparent"
                value={this.state.mobile}
                keyboardType="number-pad"
              />
              <Text style={{ paddingTop: 10, color: "black" }}>Email-id</Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ email: text })}
                placeholder="user@gmail.com"
                underlineColorAndroid="transparent"
                value={this.state.email}
              />
              <Text style={{ paddingTop: 10, color: "black" }}>Address</Text>
              <TextInput
                style={styles.fullinput}
                onChangeText={(text) => this.setState({ addres: text })}
                placeholder="lorem sum"
                underlineColorAndroid="transparent"
                value={this.state.addres}
                multiline
                numberOfLines={7}
              />
            </View>
            <Ripple rippleColor="#99FF99" style={styles.loginBtn}>
              <Text style={styles.loginText}>Save</Text>
            </Ripple>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 45,
    borderRadius: 5,
    padding: 10,
  },
  input: {
    height: hp("5%"),
    width: wp("40%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  fullinput: {
    height: hp("5%"),
    width: wp("85%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("90%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
});
