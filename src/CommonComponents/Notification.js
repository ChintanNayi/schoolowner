import PushNotification from 'react-native-push-notification';

class Notification {
    constructor() {
        PushNotification.configure({
            //Optional when TOken is generated(iOS and Android)
            onRegister: function (token) {
                console.log('Token:', token);
            },
            onNotification: function (notification) {
                console.log('Notification:', notification);
            },
            popInitialNotification: true,
            requestPermissions: false,
        });

        PushNotification.createChannel(
            {
                channelId: 'reminders', //required
                channelName: 'Task Reminder Notifications', //required
                channelDescription: 'Reminder for any tasks',
            },
            () => { },
        );
        PushNotification.getScheduledLocalNotifications(rn => {
            console.log('SN---', rn);
        });
    }
    schedulNotification(date){
        PushNotification.localNotificationSchedule({
            channelId: 'reminders',
            title: 'Reminder',
            message: 'You have set this reminder',
            date,
        });
    }
}

export default new Notification();