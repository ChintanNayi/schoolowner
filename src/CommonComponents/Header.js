import React, { useState, Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import FIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';
import { headerStyles } from '../CommonComponents/Header';

export default class Header extends Component {

    render() {
        const navigation = useNavigation();
        const [isShowing, setIsShowing] = useState(false);

        return (
            <View style={headerStyles.container}>
                <View style={headerStyles.head}>
                    <Text style={headerStyles.title}>
                        Blue Mountain Driving School
                    </Text>
                    <Image style={headerStyles.image} source={logo} />
                </View>
                <View style={headerStyles.userView}>
                    <View style={headerStyles.imgView}>
                        <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                            <Icon name="home" style={{ paddingTop: 3, paddingLeft: 5 }} color='grey' size={25} />
                        </TouchableOpacity>
                    </View>

                    <Text style={headerStyles.user}>
                        {props.title}
                    </Text>
                    <View style={headerStyles.imgView}>
                        <Avatar
                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop: 5 }}
                            rounded
                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                            onAccessoryPress={() => Alert.alert("change avatar")}
                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                            size={25}
                            onPress={() => setIsShowing(!isShowing)}
                        />
                        {isShowing === true ?
                            <View style={headerStyles.profileView}>
                                <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Avatar
                                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                            rounded
                                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                            onAccessoryPress={() => Alert.alert("change avatar")}
                                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                                            size={15}
                                            onPress={() => console.log("Works!")}
                                        />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Profile</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ borderWidth: 1, borderColor: 'grey' }} />
                                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <FIcon name="log-out" style={{ paddingTop: 3 }} color='grey' size={15} />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Log Out</Text>
                                    </View>
                                </TouchableOpacity>
                            </View> : null}
                    </View>
                </View>
            </View>
        )
    }
}
