import { StyleSheet, Dimensions, PixelRatio } from "react-native";
import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { normalize } from "react-native-elements";

var { Platform } = React;

const flashScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: hp("9%"),
    marginTop: (Platform.OS === 'ios') ? '26%' : '18%',
    marginBottom: '8%',
  },
  mainImg: {
    marginTop: '20%',
  },
  textbottom: {
    height: wp("10%"),
    color: "#696969",
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
  },
});

const EntStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 10,
  },
  head: {
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 40,
    fontSize: 25,
    color: '#4d4d4d'
  },
  subHead: {
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: '3%',
    color: '#007419',
    paddingBottom: '8%'
  },
  loginText: {
    color: 'white',
    fontWeight: '500',
  },
  loginBtn:
  {
    width: wp('80%'),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: '15%',
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('50%'),
    height: (Platform.OS === 'ios') ? hp('9%') : hp('10%'),
    marginTop: (Platform.OS === 'ios') ? '24%' : '15%',
    marginBottom: '10%',
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10 // default is 12
  },
  inputGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40
  },
  resend: {
    textAlign: 'center',
    color: '#00a64f'
  },
  resend1: {
    textAlign: 'center',
    color: 'grey'
  },
  underlineStyleBase:
  {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 3,
    color: 'black'
  },
  underlineStyleHighLighted:
  {
    borderColor: 'black',
    borderBottomColor: "#50ad50",
  },
  timerView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20
  }
});

const loginStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 30,
    paddingBottom: 30,
    fontSize: hp("4%"),
    color: "#4d4d4d",
  },
  title: {
    height: hp("4%"),
    marginBottom: 20,
    textAlign: "center",
    color: "grey",
    marginTop: 20,
  },
  input: {
    height: hp("7%"),
    width: wp("80%"),
    borderColor: "gray",
    borderBottomWidth: 1,
    margin: 10,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: hp("9%"),
    marginTop: (Platform.OS === 'ios') ? '14%' : '15%',
    marginBottom: '10%',
  },
  forgot_button: {
    textAlign: "right",
    flex: 1,
    marginRight: 50,
    marginTop: 20,
    color: "#696969",
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  forgot_right: {
    flex: 1,
    justifyContent: "flex-end",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 10,
  },
});

const getOTPStyles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    alignItems: "center",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: '12%',
    fontSize: 30,
    color: "#4d4d4d",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: '12%',
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: hp("9%"),
    marginTop: (Platform.OS === 'ios') ? '24%' : '15%',
    marginBottom: '10%',
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  radioGroup: {
    flexDirection: "row",
    width: wp("80%"),
  },
  radioText: {
    marginTop: 7,
    textAlign: "left",
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#feba24",
    alignItems: "center",
    justifyContent: "center",
  },
  checkedCircle: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: "#feba24",
  },
});

const enterOtpStyles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: "center",
    marginTop: 10,
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: '14%',
    fontSize: 25,
    color: "#4d4d4d",
  },
  input: {
    height: hp("5%"),
    width: wp("8%"),
    borderColor: "gray",
    borderBottomWidth: 1,
    borderBottomColor: "#00a64f",
    textAlign: "center",
    // marginLeft: 40,
    margin: 10,
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 3,
    color: "black",
  },
  underlineStyleHighLighted: {
    borderColor: "black",
    borderBottomColor: "#50ad50",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: hp("9%"),
    marginTop: (Platform.OS === 'ios') ? '24%' : '15%',
    marginBottom: '10%',
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  inputGroup: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 20,
  },
  resend: {
    textAlign: "center",
    color: "#00a64f",
  },
  resend1: {
    textAlign: "center",
    color: "grey",
  },
  timerView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 20,
  },
});

const forgotPswtSyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 100,
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 50,
    paddingBottom: 30,
    fontSize: hp("3%"),
    color: "#4d4d4d",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: hp("9%"),
    marginBottom: 10,
    marginTop: 5,
  },
});

const dashboardStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  head: {
    flexDirection: "row",
    marginTop: Platform.OS === "ios" ? 40 : 10,
    width: Platform.OS === "ios" ? wp("75%") : wp("80%"),
    justifyContent: "space-between",
    right: Platform.OS === "ios" ? 10 : 0,
  },
  title: {
    color: "grey",
    fontWeight: "bold",
    fontSize: normalize(17),
    right: Platform.OS === "ios" ? 15 : 18,
  },
  userView: {
    flexDirection: "row",
    marginTop: 40,
    width: wp("90%"),
    justifyContent: "space-between",
  },
  user: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
  },
  listView: {
    alignContent: 'center',
    alignItems: 'center',
    marginRight: PixelRatio.roundToNearestPixel(112),
  },
  image: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "flex-end",
    left: Platform.OS === "ios" ? '0%' : '40%',
    bottom: 5,
  },
  userImg: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "center",
    marginTop: 3,
  },
  imgView: {
    width: wp("10%"),
    height: hp("4%"),
    right: 20,
  },
  header: {
    marginTop: 55,
    width: wp("90%"),
    height: hp("5%"),
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    borderRadius: 4,
  },
  headText: {
    color: "#00a64f",
    fontSize: 20,
    fontWeight: "bold",
    paddingTop: 7,
  },
  mainView: {
    marginTop: '2%',
    borderRadius: 4,
  },
  celendarCom: {
    height: hp("10%"),
    width: wp("90%"),
    backgroundColor: "white",
    marginTop: '2%',
  },
  itemView: {
    margin: 20,
    alignItems: "center",
    shadowOffset: { width: 15, height: 15 },
    shadowOpacity: 0.2,
    shadowColor: "grey",
    shadowRadius: 10,
    borderRadius: 10,
    backgroundColor: "white",
    width: wp("35%"),
    height: PixelRatio.roundToNearestPixel(135)
    // height: (Platform.OS === 'ios') ? hp("16%") : hp("18%"),
  },
  // profileView: {
  //   width: wp("20%"),
  //   backgroundColor: "white",
  //   alignContent: "center",
  //   borderRadius: 5,
  //   right: 40,
  //   padding: 5,
  //   marginTop: 5,
  // },
  profileView: {
    width: wp("20%"),
    backgroundColor: "white",
    alignContent: "center",
    borderRadius: 5,
    right: 40,
    padding: 5,
    marginTop: Platform.OS === "ios" ? '18%' : '10%',
    shadowColor: "#000000",
    shadowOpacity: Platform.OS === "ios" ? 0.2 : 0,
    shadowRadius: Platform.OS === "ios" ? 15 : 0,
    zIndex: 2,
  },
  priceText: {
    color: "#00a64f",
    fontSize: normalize(14),
    fontWeight: "bold",
    paddingTop: 5,
    paddingBottom: 5,
  },
  btnNormal: {
    marginTop: 10,
  },
  btnPress: {
    marginTop: 5,
    paddingTop: 5,
    color: "#feba24",
    borderWidth: 1,
    width: wp("15%"),
    borderRadius: 3,
    borderColor: "#feba24",
    alignItems: "center",
    shadowOffset: { width: 15, height: 15 },
    shadowOpacity: 1,
    shadowColor: "grey",
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "left",
  },
  modalText: {
    textAlign: "left",
    padding: 10,
    fontWeight: "bold",
    color: "black",
    fontSize: 15,
  },
  modalLable: {
    textAlign: "left",
    marginLeft: 10,
    paddingTop: 10,
    color: "black",
    fontWeight: "bold",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00a64f",
    marginLeft: 10,
    top: 10,
  },
});

const headerStyles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: "center",
  },
  head: {
    flexDirection: "row",
    marginTop: Platform.OS === "ios" ? 50 : 10,
    width: wp("80%"),
    justifyContent: "space-between",
    marginLeft: 30,
  },
  title: {
    color: "grey",
    fontWeight: "bold",
    fontSize: 20,
  },
  image: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "flex-end",
    marginLeft: 20,
  },
  userView: {
    flexDirection: "row",
    marginTop: 15,
    width: wp("90%"),
    justifyContent: "space-between",
    paddingBottom: 10,
  },
  user: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
    paddingTop: 5,
    color: "#00a64f",
  },
  userImg: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "center",
    marginTop: 3,
  },
  imgView: {
    backgroundColor: "#FFFFFF",
    width: wp("10%"),
    height: hp("4%"),
    borderRadius: 4,
  },
  profileView: {
    width: wp("20%"),
    backgroundColor: "white",
    alignContent: "center",
    borderRadius: 5,
    top: 30,
    right: 0,
    padding: 5,
    marginTop: 5,
    position: "absolute",
  },
});

export {
  flashScreenStyles,
  loginStyles,
  getOTPStyles,
  enterOtpStyles,
  forgotPswtSyles,
  dashboardStyles,
  headerStyles,
  EntStyles,
};
