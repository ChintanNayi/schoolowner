import axios from "axios";
import {
    LOGIN_OWNER,
    LOGIN_OWNER_ERROR,
    GET_OTP,
    GET_OTP_ERROR,
    ENTER_OTP,
    ENTER_OTP_ERROR,
    FORGOT_PASSWORD, FORGOT_PASSWORD_ERROR,
    FORGOT_PASSWORD_OTP, FORGOT_PASSWORD_OTP_ERROR,
    ENTER_NEW_PASSWORD, ENTER_NEW_PASSWORD_ERROR,
} from "../action-types";

import {
    OWNER_LOGIN_API,
    OWNER_SEND_OTP_API,
    OWNER_VERIFY_OTP_API,
    RESET_PASSWORD_API,
    RESET_PASSWORD_SEND_OTP_EMAIL_API,
    RESET_PASSWORD_VERIFY_OTP_API,
    config
} from "../const";

export const loginOwner = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(OWNER_LOGIN_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: LOGIN_OWNER,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: LOGIN_OWNER_ERROR,
                    payload: error,
                });
            });
    }
}

export const loginOTP = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(OWNER_SEND_OTP_API, body, config)
            .then(response => {
                // console.log('OTP Action Log', response);
                dispatch({
                    type: GET_OTP,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log('OTP Action Error Log', error);
                dispatch({
                    type: GET_OTP_ERROR,
                    payload: error,
                });
            });
    }
}

export const enterOTP = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(OWNER_VERIFY_OTP_API, body, config)
            .then(response => {
                // console.log('OTP Action Log', response);
                dispatch({
                    type: ENTER_OTP,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log('OTP Action Error Log', error);
                dispatch({
                    type: ENTER_OTP_ERROR,
                    payload: error,
                });
            });
    }
}

export const forgotPassword = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_SEND_OTP_EMAIL_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: FORGOT_PASSWORD,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: FORGOT_PASSWORD_ERROR,
                    payload: error,
                });
            });
    }
}

export const forgotPasswordOTP = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_VERIFY_OTP_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: FORGOT_PASSWORD_OTP,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: FORGOT_PASSWORD_OTP_ERROR,
                    payload: error,
                });
            });
    }
}

export const newPassword = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: ENTER_NEW_PASSWORD,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: ENTER_NEW_PASSWORD_ERROR,
                    payload: error,
                });
            });
    }
}