import {
    LOGIN_OWNER, LOGIN_OWNER_ERROR,
    GET_OTP, GET_OTP_ERROR,
    ENTER_OTP, ENTER_OTP_ERROR,
    FORGOT_PASSWORD, FORGOT_PASSWORD_ERROR,
    FORGOT_PASSWORD_OTP, FORGOT_PASSWORD_OTP_ERROR,
    ENTER_NEW_PASSWORD, ENTER_NEW_PASSWORD_ERROR
} from "../action-types";


const initialState = {
    login: null,
    oneTime: null,
    otp: null,
    forgot: null,
    passOTP: null,
    newPassword: null,
}

const loginStore = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_OWNER: {
            return {
                ...state,
                login: action.payload,
            };
        };
        case LOGIN_OWNER_ERROR: {
            return {
                ...state,
                login: null,
            };
        };
        case GET_OTP: {
            return {
                ...state,
                oneTime: action.payload,
            };
        };
        case GET_OTP_ERROR: {
            return {
                ...state,
                oneTime: null,
            };
        };
        case ENTER_OTP: {
            return {
                ...state,
                otp: action.payload,
            };
        };
        case ENTER_OTP_ERROR: {
            return {
                ...state,
                otp: null,
            };
        };
        case FORGOT_PASSWORD: {
            return {
                ...state,
                forgot: action.payload,
            };
        };
        case FORGOT_PASSWORD_ERROR: {
            return {
                ...state,
                forgot: null,
            };
        };
        case FORGOT_PASSWORD_OTP: {
            return {
                ...state,
                passOTP: action.payload,
            };
        };
        case FORGOT_PASSWORD_OTP_ERROR: {
            return {
                ...state,
                passOTP: null,
            };
        };
        case ENTER_NEW_PASSWORD: {
            return {
                ...state,
                newPassword: action.payload,
            };
        };
        case ENTER_NEW_PASSWORD_ERROR: {
            return {
                ...state,
                newPassword: null,
            };
        };
        default:
            return state;
    }
}

export default loginStore;