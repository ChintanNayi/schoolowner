import { combineReducers, applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from 'redux-thunk';

//Login Reducers
import loginVerify from "./loginReducer";
import enterOTP from "./loginReducer";
import typeOTP from "./loginReducer";
import forgotPasswordStore from "./loginReducer";
import forgotPasswordOTPStore from "./loginReducer";
import newPasswordStore from "./loginReducer";

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));

const rootReducer = combineReducers({
    loginReducer: loginVerify,
    getOTPReducer: enterOTP,
    enterOTPReducer: typeOTP,
    forgotPasswordReducer: forgotPasswordStore,
    forgotPasswordOTPReducer: forgotPasswordOTPStore,
    newPasswordReducer: newPasswordStore
});

const configureStore = () => createStore(rootReducer, composedEnhancer);

export default configureStore;