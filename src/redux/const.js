import API_END_POINT from '../../EndPoint';
export const LIMIT = 10;
export const PAGE = 1;
export const config = {
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    },
};

// LOGIN PAGES API
export const OWNER_LOGIN_API = API_END_POINT + "";
export const OWNER_SEND_OTP_API = API_END_POINT + "";
export const OWNER_VERIFY_OTP_API = API_END_POINT + "";
export const RESET_PASSWORD_SEND_OTP_EMAIL_API = API_END_POINT + "";
export const RESET_PASSWORD_VERIFY_OTP_API = API_END_POINT + "";
export const RESET_PASSWORD_API = API_END_POINT + "";
