import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, ScrollView } from "react-native";
import LOGO from "../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import baseURL from "../API/api";
import axios from "axios";
import { EntStyles } from "../CommonComponents/css";
import { Spinner } from "native-base";
import { Icon } from 'react-native-elements';
import Toast from 'react-native-toast-message';

import { connect } from "react-redux";
import { forgotPasswordOTP } from "../redux/actions/loginAction";

export class ForgotPasswordOTP extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            eff: false,
            timer: null,
            counter: 59,
            phone: "+919924460329",
            confirmResult: null,
            verificationCode: "",
            userId: "",
        };
    }

    //Toast Message
    showToast = () =>
        Toast.show({
            visibilityTime: 2000,
            position: 'top',
            text1: 'OTP Verified Successfully!',
            topOffset: Platform.OS === 'ios' ? 40 : 5,
        });

    componentDidMount() {
        this.startTimer();
    }

    handleVerifyCode = async () => {
        await this.props.dispatchState(this.state.comment);

        this.setState({ loaded: true });
        setTimeout(() => {
            this.setState({ loaded: false });
        }, 2500);
        this.setState({ eff: true, isVisible: true });

        this.showToast();
        setTimeout(() => {
            // Add your logic for the transition
            this.setState({ isVisible: false, eff: false });
            this.props.navigation.navigate("NewPassword");
        }, 3000);
        // Request for OTP verification
        // const nav = this.props.navigation
        // console.log("otp",this.state.verificationCode)
        // nav.navigate('ScreenExternal');
        // axios.post(`${baseURL}`+`auth/user/signin/otp/email/verify`,{ email:'chintan.vibrantsofttech@gmail.com',otp:"073023"})
        //     .then(function (response) {
        //         // handle success
        //         console.log("respo",response)
        //         if (response.status === 200) {
        //             nav.navigate('ScreenExternal');
        //         }
        //     })
        //     .catch(function (error) {
        //         console.log("error", error)
        //     });
    };

    startTimer = () => {
        this.setState({
            show: false,
        });
        let timer = setInterval(this.manageTimer, 1000);
        this.setState({ timer });
    };

    manageTimer = () => {
        var states = this.state;

        if (states.counter === 0) {
            clearInterval(this.state.timer);
            this.setState({
                counter: 59,
                show: true,
            });
        } else {
            this.setState({
                counter: this.state.counter - 1,
            });
        }
    };

    componentWillUnmount() {
        clearInterval(this.state.timer);
    }

    render() {
        return (
            <ScrollView>
                <View style={EntStyles.container}>
                    <View style={{ alignItems: "center" }}>
                        <Image style={EntStyles.image} source={LOGO} />
                    </View>
                    <Text style={EntStyles.head}>Please Enter The 6-Digit Code</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon containerStyle={{ top: 11, paddingRight: 5 }} name='check' type='font-awesome' size={17} color='#007419'></Icon>
                        <Text style={EntStyles.subHead}>Check your Email for the OTP</Text>
                    </View>
                    <OTPInputView
                        autoFocusOnLoad
                        onCodeChanged={(code) => {
                            this.setState({ verificationCode: code });
                        }}
                        placeholderCharacter="0"
                        placeholderTextColor="grey"
                        style={{ width: "70%", height: 50 }}
                        pinCount={6}
                        codeInputFieldStyle={EntStyles.underlineStyleBase}
                        codeInputHighlightStyle={EntStyles.underlineStyleHighLighted}
                    />

                    {this.state.loaded === true ? (
                        <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                    ) : (
                        <Ripple
                            rippleColor="#99FF99"
                            rippleOpacity={0.9}
                            onPress={() => this.handleVerifyCode()}
                            style={EntStyles.loginBtn}
                        >
                            <Text style={EntStyles.loginText}>CONTINUE</Text>
                        </Ripple>
                    )}

                    {this.state.show === true ? (
                        <View style={EntStyles.timerView}>
                            <Text style={{ textAlign: "center", marginRight: 10 }}>
                                00:00
                            </Text>
                            <TouchableOpacity onPress={() => this.startTimer()}>
                                <Text style={EntStyles.resend}>Resend Code</Text>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <View style={EntStyles.timerView}>
                            <Text style={{ textAlign: "center", marginRight: 10 }}>
                                00:{this.state.counter}
                            </Text>
                            <Text style={EntStyles.resend1}>Resend Code</Text>
                        </View>
                    )}
                </View>
                <Toast />
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    const { passOTP } = state.forgotPasswordOTPReducer;
    return { forgotPassOTPState: passOTP };
}

const mapDispatchToProps = (dispatch) => (
    {
        dispatchState: (email) => dispatch(forgotPasswordOTP(email))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordOTP);
