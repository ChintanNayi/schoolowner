import React, { Component } from 'react'
import { Button, View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import axios from 'axios';
import { SafeAreaView } from 'react-native-safe-area-context';
import LOGO from '../Assets/dsc-main-logo.png';
import { Input } from 'react-native-elements';
import { Spinner } from 'native-base';
import Ripple from 'react-native-material-ripple';
import Toast from 'react-native-toast-message';

import { connect } from 'react-redux';
import { forgotPassword } from '../redux/actions/loginAction';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // marginTop: 10,
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 50,
        paddingBottom: 30,
        fontSize: hp('3%'),
        color: '#4d4d4d'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: (Platform.OS === 'ios') ? '16%' : '18%',
        marginBottom: '10%'
    },
});

export class ForgotPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            isValidEmail: true,
        }
    }

    showToast = () =>
        Toast.show({
            visibilityTime: 3000,
            position: 'top',
            text1: 'OTP Sent Successfully!',
            topOffset: Platform.OS === 'ios' ? 40 : 5,
        });

    CallToAction = async (text) => {
        this.setState({ loaded: true });
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (
            reg.test(this.state.email) === true
        ) {
            await this.props.stateDispatch(this.state.email);
            this.setState({
                email: text,
                isValidEmail: true,

            });
            this.showToast();
            setTimeout(() => {
                this.setState({ loaded: false });
                this.props.navigation.navigate("ForgotPasswordOTP");
            }, 4000);
        } else {
            this.setState({
                email: text,
                isValidEmail: false,
            });
            this.setState({ loaded: false });
        }

        // SignIn = () => {
        //     const nav = this.props.navigation
        //     const ldata = { email: this.state.email, password: this.state.password }
        //     const headers = {
        //         'Content-Type': 'multipart/form-data'
        //     }

        //     nav.navigate('SendOTP');

        // axios
        //     .post('https://vibrantsofttech.com/project/prosourceadvisory/api/authentication/login', ldata, {
        //         headers: headers
        //     })
        //     .then(function (response) {
        //         // handle success
        //         console.log("res", response.data.status)

        //         if (response.data.status === true) {
        //             nav.navigate('SendOTP');
        //             console.log("ifff")
        //         }
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         alert(error.message);
        //     });
    }

    render() {
        return (
            <ScrollView>
                <SafeAreaView style={styles.container}>

                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} /></View>
                    <Text style={styles.head}>Forgot Password</Text>
                    <View style={{ width: wp('84%') }}>
                        <Input
                            // containerStyle={{ width: '85%' }}
                            style={{ fontSize: 14 }}
                            placeholder="Enter User Name"
                            leftIcon={{ type: 'font-awesome', name: 'user-o', color: 'grey' }}
                            onChangeText={(text) => this.setState({ email: text })}
                            errorMessage={this.state.isValidEmail ? null : ('Please enter a valid username.')}
                        />
                    </View>

                    {this.state.loaded === true ? (
                        <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                    ) : (
                        <Ripple
                            rippleColor="#99FF99"
                            rippleOpacity={0.9}
                            onPress={() => this.CallToAction()}
                            style={styles.loginBtn}
                        >
                            <Text style={styles.loginText}>Call To Action</Text>
                        </Ripple>
                    )}
                </SafeAreaView>
                <Toast />
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    const { forgot } = state.forgotPasswordReducer;
    return { forgotState: forgot };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (email) => dispatch(forgotPassword(email))
});


export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
