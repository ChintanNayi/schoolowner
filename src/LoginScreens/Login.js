import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import LOGO from "../Assets/dsc-main-logo.png";
import { Input, Icon } from "react-native-elements";
import Ripple from "react-native-material-ripple";
import { loginStyles } from "../CommonComponents/css";
import { Spinner } from "native-base";
import Toast from 'react-native-toast-message';

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      loaded: false,
      eff: false,
      isVisible: false,
      isValidEmail: true,
      isValidPassword: true,
      isSecureEntry: true,
      email: "",
      password: "",
    };
  }

  //Toast Message
  showToast = () =>
    Toast.show({
      visibilityTime: 2000,
      position: 'top',
      text1: 'You have logged in successfully!',
      topOffset: Platform.OS === 'ios' ? 40 : 5,
    });

  //Login Function
  SignIn = () => {
    this.setState({ loaded: true });
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      reg.test(this.state.email) === true &&
      this.state.password.trim().length >= 8
    ) {
      this.setState({
        isValidEmail: true,
        isValidPassword: true,
      });
      this.showToast();
      setTimeout(() => {
        this.setState({ loaded: false });
        this.props.navigation.navigate("SendOTP");
      }, 3000);
    } else {
      this.setState({
        isValidEmail: false,
        isValidPassword: false,
      });
      this.setState({ loaded: false });
    }

    // const nav = this.props.navigation;
    // axios
    //     .post(`${baseURL}` + `auth/user/signin`, { email: this.state.email, password: this.state.password })
    //     .then(function (response) {
    //         // handle success
    //         if (response.status === 200) {
    //             nav.navigate('SendOTP');
    //         }
    //     })
    //     .catch(function (error) {
    //         // handle error
    //         console.log("error", error)
    //     });
  };

  render() {
    return (
      <ScrollView>
        <SafeAreaView style={loginStyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={loginStyles.image} source={LOGO} />
          </View>

          <Text style={loginStyles.head}>Sign In</Text>

          <Input
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 14 }}
            placeholder="Enter User Name"
            leftIcon={{ type: 'font-awesome', name: 'user-o', color: 'grey' }}
            onChangeText={(text) => this.setState({ email: text })}
            errorMessage={this.state.isValidEmail ? null : ('Please enter a valid username.')}
          />
          {/* {this.state.isValidEmail ? null : (
            <View style={{ paddingRight: 188 }}>
              <Text style={loginStyles.errorMsg}>
                Please enter a valid username.
              </Text>
            </View>
          )} */}

          <Input
            secureTextEntry={this.state.isSecureEntry}
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 15, right: 20 }}
            placeholder="Enter Password"
            leftIcon={{ type: 'evilicon', name: 'unlock', size: 40, color: 'grey' }}
            leftIconContainerStyle={{ right: 10 }}
            rightIcon={
              <TouchableOpacity onPress={() => this.setState({ isSecureEntry: !this.state.isSecureEntry })}>
                <Icon name='eye' type='font-awesome' size={20} color='grey'>{this.state.isSecureEntry}</Icon>
              </TouchableOpacity>
            }
            onChangeText={(text) => this.setState({ password: text })}
            errorMessage={this.state.isValidPassword ? null : ('Password must be at least 8 characters.')}
          />
          {/* {this.state.isValidPassword ? null : (
            <View style={{ paddingRight: 145 }}>
              <Text style={loginStyles.errorMsg}>
                Password must be at least 8 characters.
              </Text>
            </View>
          )} */}

          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("ForgotPassword")}
              style={loginStyles.forgot_right}
            >
              <Text style={loginStyles.forgot_button}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              onPress={() => this.SignIn()}
              style={loginStyles.loginBtn}
            >
              <Text style={loginStyles.loginText}>SIGN IN</Text>
            </Ripple>
          )}
        </SafeAreaView>
        <Toast />
      </ScrollView>
    );
  }
}

export default Login;
