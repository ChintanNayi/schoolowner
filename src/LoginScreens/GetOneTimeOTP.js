import React, { Component } from "react";
import {
  Button,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
} from "react-native";
import LOGO from "../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import { getOTPStyles } from "../CommonComponents/css";
import { Spinner } from "native-base";
import { connect } from "react-redux";
import { loginOTP } from '../redux/actions/loginAction';
import Toast from 'react-native-toast-message';

export class GetOneTimeOTP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      loaded: false,
      checked: "first",
      otpState: "",
      options: [
        {
          key: "pay",
          text: "Email Id",
        },
        {
          key: "performance",
          text: "Phone Number",
        },
      ],
      selectedOption: "pay",
    };
    this.onSelect = this.onSelect.bind(this);
  }

  //Toast Message
  showToast = () =>
    Toast.show({
      visibilityTime: 2000,
      position: 'top',
      text1: 'OTP Sent Successfully!',
      topOffset: Platform.OS === 'ios' ? 40 : 5,
    });

  //Get selected option function
  onSelect = (item) => {
    this.setState({ selectedOption: item });
    console.log("test", this.state.selectedOption, item);
    if (this.state.selectedOption === item.key) {
      this.setState({ selectedOption: null });
    } else {
      this.setState({ selectedOption: item.key });
    }
  };

  //Send OTP Function
  sendOTP = async () => {
    await this.props.dispatchState(this.state.email);

    this.setState({ loaded: true });
    // if (!this.props.otpState.error) {
      this.showToast();
      setTimeout(() => {
        this.setState({ loaded: false });
        this.props.navigation.navigate("EnterOTP");
      }, 3000);
    // }
  };

  render() {
    return (
      <ScrollView>
        <View style={getOTPStyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={getOTPStyles.image} source={LOGO} />
          </View>
          <Text style={getOTPStyles.head}>Get One-Time Code</Text>
          <View
            style={{ alignSelf: "flex-start", marginLeft: 60, marginTop: 80 }}
          >
            {this.state.options.map((item) => {
              return (
                <View key={item.key} style={getOTPStyles.buttonContainer}>
                  <TouchableOpacity
                    style={getOTPStyles.circle}
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    {this.state.selectedOption === item.key && (
                      <View style={getOTPStyles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    <Text style={{ marginLeft: 20, color: "black" }}>
                      {item.text}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              onPress={() => this.sendOTP()}
              style={getOTPStyles.loginBtn}
            >
              <Text style={getOTPStyles.loginText}>Send</Text>
            </Ripple>
          )}
        </View>
        <Toast />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { oneTime } = state.getOTPReducer;
  return { otpState: oneTime };
}

const mapDispatchToProps = (dispatch) => (
  {
    dispatchState: (email) => dispatch(loginOTP(email))
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(GetOneTimeOTP);
