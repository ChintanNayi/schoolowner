import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Platform,
  ScrollView,
} from "react-native";
import LOGO from "../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { enterOtpStyles } from "../CommonComponents/css";
import AsyncStorage from "@react-native-community/async-storage";
import { Spinner } from "native-base";
import Toast from 'react-native-toast-message';

import { enterOTP } from '../redux/actions/loginAction';
import { connect } from "react-redux";
import OTPTextView from "react-native-otp-textinput";

export class EnterOTP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      eff: false,
      isVisible: false,
      timer: null,
      counter: 59,
      phone: "+919924460329",
      confirmResult: null,
      verificationCode: "",
      userId: "",
      comment: "",
      enterOTPState: "",
    };
  }

  componentDidMount() {
    this.startTimer();
  }

  startTimer = () => {
    this.setState({
      show: false,
    });
    let timer = setInterval(this.manageTimer, 1000);
    this.setState({ timer });
  };

  manageTimer = () => {
    var states = this.state;
    if (states.counter === 0) {
      clearInterval(this.state.timer);
      this.setState({
        counter: 59,
        show: true,
      });
    } else {
      this.setState({
        counter: this.state.counter - 1,
      });
    }
  };

  componentWillUnmount() {
    clearInterval(this.state.timer);
  }

  //Toast Message
  showToast = () =>
    Toast.show({
      visibilityTime: 2000,
      position: 'top',
      text1: 'OTP Verified Successfully!',
      topOffset: Platform.OS === 'ios' ? 40 : 5,
    });

  handleVerifyCode = async () => {
    await this.props.dispatchState(this.state.comment);

    this.setState({ loaded: true });
    // Request for OTP verification
    this.setState({ eff: true, isVisible: true });
    AsyncStorage.setItem("isLogin", "login");
    // if (!this.props.enterOTPState.error) {
    this.showToast();
    setTimeout(() => {
      // Add your logic for the transition
      this.setState({ loaded: false });
      this.setState({ isVisible: false, eff: false });
      this.props.navigation.navigate("Dashboard");
    }, 3000);
    // }
  };

  render() {
    return (
      <ScrollView>
        <View style={enterOtpStyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={enterOtpStyles.image} source={LOGO} />
          </View>
          <Text style={enterOtpStyles.head}>Please Enter The 6-Digit Code</Text>
          <View style={enterOtpStyles.inputGroup}>
            <OTPTextView
              keyboardType="numeric"
              onCodeChanged={(code) => this.setState({ comment: code })}
              textInputStyle={{
                width: 40,
                height: 50,
              }}
              inputCount={6}
            />
          </View>

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              onPress={() => this.handleVerifyCode()}
              style={enterOtpStyles.loginBtn}
            >
              <Text style={enterOtpStyles.loginText}>CONTINUE</Text>
            </Ripple>
          )}

          {this.state.show === true ? (
            <View style={enterOtpStyles.timerView}>
              <Text style={{ textAlign: "center", marginRight: 10 }}>
                00:00
              </Text>
              <TouchableOpacity onPress={() => this.startTimer()}>
                <Text style={enterOtpStyles.resend}>Resend Code</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={enterOtpStyles.timerView}>
              <Text style={{ textAlign: "center", marginRight: 10 }}>
                00:{this.state.counter}
              </Text>
              <Text style={enterOtpStyles.resend1}>Resend Code</Text>
            </View>
          )}
        </View>
        <Toast />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { otp } = state.enterOTPReducer;
  return { enterOTPState: otp };
}

const mapDispatchToProps = (dispatch) => (
  {
    dispatchState: (email) => dispatch(enterOTP(email))
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(EnterOTP);
