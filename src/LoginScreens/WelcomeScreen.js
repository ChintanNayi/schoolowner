import React, { Component } from "react";
import { Image, View, TouchableOpacity, Text } from "react-native";
import logo from "../Assets/dsc-main-logo.png";
import WelcomeImg from "../Assets/welcome.png";
import Ripple from "react-native-material-ripple";
import { flashScreenStyles } from "../CommonComponents/css";
import AsyncStorage from "@react-native-community/async-storage";

export default class WelcomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      AsyncStorage.getItem("isLogin").then((value) =>
        this.props.navigation.replace(value === null ? "Login" : "Dashboard")
      );
    }, 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <View style={flashScreenStyles.container}>
        <Image style={flashScreenStyles.image} source={logo} />
        <Image style={flashScreenStyles.mainImg} source={WelcomeImg} />

        <View style={flashScreenStyles.bottom}>
          <Text style={flashScreenStyles.textbottom}>
            @ 2021 Driving School Cloud. All rights reserved
          </Text>
        </View>
      </View>
    );
  }
}
